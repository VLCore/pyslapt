#define _GNU_SOURCE
#include <slapt.h>


/***************************************************************************
 *                                                                         *
 * pyslapt.c - A helper C library for the libslapt python bindings.        *
 *                                                                         *
 * Written by rbistolfi                                                    *
 * December 2010                                                           *
 *                                                                         *
 * Released under the GNU General Public License                           *
 *                                                                         *
 * The purpose of this library is to provide helper functions for a        *
 * ctypes based python binding of libslapt, by Jaos.                       *
 * The ctypes module can't resolve names provided by external libraries.   *
 * By compiling this library and linking it against curl and libslapt we   *
 * allow the symbols defined here to be exported to python through         *
 * the ctypes module. Also, we need some helper functions. For example,    *
 * there is no equivalent of the C FILE type in ctypes, therefore we       *
 * need to provide a helper function for avoiding the need of passing a    *
 * FILE object as argument, by taking a file path instead.                 *
 *                                                                         *
 ***************************************************************************/


/* slapt_get_pkg_list_from_filepath
 *
 * Arguments:
 *      - char filepath: a string indicating a path to a 
 *      PACKAGES.TXT like file
 *
 * Returns:
 *      - A pointer to a slapt_pkg_list structure
 */


slapt_pkg_list_t *
slapt_get_pkg_list_from_filepath(char *filepath) 
{
    FILE *data_f = fopen(filepath, "r");
    slapt_pkg_list_t *packages = slapt_parse_packages_txt(data_f);
    return packages;
}


/* slapt_set_download_callback
 *
 * Set the download callback function
 *
 * Arguments:
 *      - A pointer to the callback
 *
 * Returns:
 *      - A slapt_rc_config
 */

slapt_rc_config *  
slapt_set_download_callback(int (*progress_cb) (void *,double,double,double,double))
{
    slapt_rc_config *config = slapt_read_rc_config("/etc/slapt-get/slapt-getrc");
    config->progress_cb = progress_cb;
    return config;
}


int main() 
{
    return (0);
}
