from distutils.core import setup

version = '0.0.1'

setup(name='pyslapt',
      version=version,
      description="Python binding for libslapt",
      long_description="""\
Python binding for libslapt""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='Slackware VectorLinux slapt-get gslapt',
      author='Rodrigo Bistolfi',
      author_email='rbistolfi@gmail.com',
      url='vlcore.vectorlinux.com',
      license='GPL',
      packages=["pyslapt"],
      )
