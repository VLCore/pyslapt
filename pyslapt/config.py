#-*- coding: utf8 -*-


from ctypes import *
from .libslapt import libslapt
#import finalize #recipe 577242


__all__ = ["RCConfig", "global_config"]
__author__ = "rbistolfi"
__version__ = "0.0.1"


WORKINGDIR_TOKEN_LEN = 256
PROGRESS_CB_T = CFUNCTYPE(POINTER(None), POINTER(None), c_double, c_double, 
        c_double, c_double)
 

class SlaptListStruct(Structure):
    """Structure for slapt_list_t
    
    """
    _fields_ = [ ("items", POINTER(c_char_p)), ("count", c_int) ]


SlaptListPointer = POINTER(SlaptListStruct)


class SourceStruct(Structure):
    """Structure for slapt_source_t
    
    """
    _fields_ = [ ("url", c_char_p), ("priority", c_uint), ("disabled", c_bool) ]


SourcePointer = POINTER(SourceStruct)


class SourceListStruct(Structure):
    """Structure for slapt_source_list_t
    
    """
    _fields_ = [ ("src", POINTER(SourcePointer)), ("count", c_uint) ]


SourceListPointer = POINTER(SourceListStruct)


class RCConfigStruct(Structure):
    """Structure for slapt_pkg_info_t   

    typedef struct {
      char working_dir[WORKINGDIR_TOKEN_LEN];
      slapt_source_list_t *sources;
      slapt_list_t *exclude_list;
      int (*progress_cb) (void *,double,double,double,double);
      SLAPT_BOOL_T download_only;
      SLAPT_BOOL_T dist_upgrade;
      SLAPT_BOOL_T simulate;
      SLAPT_BOOL_T no_prompt;
      SLAPT_BOOL_T re_install;
      SLAPT_BOOL_T ignore_excludes;
      SLAPT_BOOL_T no_md5_check;
      SLAPT_BOOL_T ignore_dep;
      SLAPT_BOOL_T disable_dep_check;
      SLAPT_BOOL_T print_uris;
      SLAPT_BOOL_T dl_stats;
      SLAPT_BOOL_T remove_obsolete;
      SLAPT_BOOL_T no_upgrade;
      unsigned int retry;
    } slapt_rc_config;

    """
    _fields_ = [ ("working_dir", c_char * WORKINGDIR_TOKEN_LEN),
            ("sources", SourceListPointer),
            ("exclude_list", SlaptListPointer),
            ("progress_cb", PROGRESS_CB_T),
            ("download_only", c_bool),
            ("dist_upgrade", c_bool),
            ("simulate", c_bool),
            ("no_prompt", c_bool),
            ("re_install", c_bool),
            ("ignore_excludes", c_bool),
            ("no_md5_check", c_bool),
            ("ignore_dep", c_bool),
            ("disable_dep_check", c_bool),
            ("print_uris", c_bool),
            ("dl_stats", c_bool),
            ("remove_obsolete", c_bool),
            ("no_upgrade", c_bool),
            ("retry", c_uint) ]


RCConfigPointer = POINTER(RCConfigStruct)


slapt_read_rc_config = libslapt.slapt_read_rc_config
slapt_read_rc_config.restype = RCConfigPointer


class RCConfig(object):

    global_config = "/etc/slapt-get/slapt-getrc"

    def __init__(self, rc_config_pointer=None):
        """PackageList initialization from a slapt_pkg_list pointer
        (PackageListPointer)
        
        """
        if rc_config_pointer is None:
            rc_config_pointer = slapt_read_rc_config(self.global_config)
        self._pointer = self._as_parameter_ = rc_config_pointer
        self._contents = rc_config_pointer.contents

    def __getattr__(self, name):
        return getattr(self._contents, name)

    @property
    def progress_cb(self):
        """Return the configured callbacl for curl calls
        
        """
        return self._contents.progress_cb

    @progress_cb.setter
    def progress_cb(self, func):
        """Set callback for download progress
        
        """
        self.set_progress_callback(func)

    def set_progress_callback(self, func):
        """The function must return None and take five arguments. The first
        one is a pointer to the curl client, you can ignore it. 
        
        """
        #void *clientp, double dltotal, double dlnow, double ultotal, double ulnow
        cb = PROGRESS_CB_T(func)
        self._contents.progress_cb = cb


global_config = RCConfig()
