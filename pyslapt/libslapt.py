#-*- coding: utf8 -*-


"""Basic C Types from libslapt

"""


from ctypes import *
#import finalize #recipe 577242


__author__ = "rbistolfi"
__version__ = "0.0.1"


LEN_MD5 = 33 # do not forget the null char


class PackageStruct(Structure):
    """Structure for slapt_pkg_info_t
    
    """
    _fields_ = [ ("md5", c_char * LEN_MD5),
            ("name", c_char_p),
            ("version", c_char_p),
            ("mirror", c_char_p),
            ("location", c_char_p),
            ("description", c_char_p),
            ("required", c_char_p),
            ("conflicts", c_char_p),
            ("suggests", c_char_p),
            ("file_ext", c_char_p),
            ("size_c", c_uint),
            ("size_u", c_uint),
            ("priority", c_uint),
            ("installed", c_int) ]


PackagePointer = POINTER(PackageStruct)


class PackageListStruct(Structure):
    """Structure for slapt_pkg_list
    
    """
    _fields_ = [ ("pkgs", POINTER(PackagePointer)),
            ("pkg_count", c_uint),
            ("free_pkgs", c_int),
            ("ordered", c_int) ]


PackageListPointer = POINTER(PackageListStruct)


class PackageErrorStruct(Structure):
    """Structure for slapt_pkg_err_t
    
    """
    _fields_ = [("pkg", c_char_p), ("error", c_char_p)]


PackageErrorPointer = POINTER(PackageErrorStruct)


class PackageErrorListStruct(Structure):
    """Structure for slapt_pkg_err_list_t
    
    """
    _fields_ = [("errs", POINTER(PackageErrorPointer)), ("err_count", c_uint)]


PackageErrorListPointer = POINTER(PackageErrorListStruct)


## Slapt C functions ########


libslapt = CDLL("libpyslapt.so")

slapt_get_newest_pkg = libslapt.slapt_get_newest_pkg
slapt_get_newest_pkg.restype = PackagePointer

slapt_get_exact_pkg = libslapt.slapt_get_exact_pkg
slapt_get_exact_pkg.restype = PackagePointer

slapt_get_pkg_by_details = libslapt.slapt_get_pkg_by_details
slapt_get_pkg_by_details.restype = PackagePointer

slapt_init_pkg_list = libslapt.slapt_init_pkg_list
slapt_init_pkg_list.restype = PackageListPointer

slapt_get_installed_pkgs = libslapt.slapt_get_installed_pkgs
slapt_get_installed_pkgs.restype = PackageListPointer

slapt_get_available_pkgs = libslapt.slapt_get_available_pkgs
slapt_get_available_pkgs.restype = PackageListPointer

slapt_get_pkg_conflicts = libslapt.slapt_get_pkg_conflicts
slapt_get_pkg_conflicts.restype = PackageListPointer

slapt_is_required_by = libslapt.slapt_is_required_by
slapt_is_required_by.restype = PackageListPointer

slapt_search_pkg_list = libslapt.slapt_search_pkg_list
slapt_search_pkg_list.restype = PackageListPointer

slapt_get_pkg_list_from_filepath = libslapt.slapt_get_pkg_list_from_filepath
slapt_get_pkg_list_from_filepath.restype = PackageListPointer

slapt_get_pkg_dependencies = libslapt.slapt_get_pkg_dependencies
slapt_get_pkg_dependencies.restype = c_int

slapt_is_excluded = libslapt.slapt_is_excluded
slapt_is_excluded.restype = c_int

slapt_cmp_pkgs = libslapt.slapt_cmp_pkgs
slapt_cmp_pkgs.restype = c_int

slapt_stringify_pkg = libslapt.slapt_stringify_pkg
slapt_stringify_pkg.restype = c_char_p

slapt_get_pkg_filelist = libslapt.slapt_get_pkg_filelist
slapt_get_pkg_filelist.restype = c_char_p

slapt_init_transaction = libslapt.slapt_init_transaction

slapt_add_deps_to_trans = libslapt.slapt_add_deps_to_trans

slapt_add_install_to_transaction = libslapt.slapt_add_install_to_transaction

slapt_handle_transaction = libslapt.slapt_handle_transaction

slapt_read_rc_config = libslapt.slapt_read_rc_config

slapt_download_pkg = libslapt.slapt_download_pkg

slapt_set_download_callback = libslapt.slapt_set_download_callback

slapt_free_pkg = libslapt.slapt_free_pkg

slapt_free_pkg_list = libslapt.slapt_free_pkg_list

