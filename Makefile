# Makefile for pyslapt

VERSION = 0.0.1

PREFIX?=/usr
DESTDIR?=
LIBDIR?=$(PREFIX)/lib

CURL_LIBS=`curl-config --libs`
GPGME_LIBS=`gpgme-config --libs`

CFLAGS=-fPIC

CC=/usr/bin/gcc
INSTALL=/usr/bin/install -c -D
PYTHON=/usr/bin/python

TARGET=libpyslapt.so

lib: libpyslapt.c
	$(CC) $(CURL_LIBS) $(GPGME_LIBS) -lslapt -shared -o $(TARGET) libpyslapt.c $(CFLAGS)

clean:
	rm -fr $(TARGET)
	rm -fr build
	rm -fr dist
	rm -fr pyslapt.egg-info
	rm -fr *pyc

install:
	$(PYTHON) setup.py build
ifdef DESTDIR
	$(PYTHON) setup.py install --root=$(DESTDIR)
else
	$(PYTHON) setup.py install
endif
	$(INSTALL) $(TARGET) $(DESTDIR)$(LIBDIR)/$(TARGET)
